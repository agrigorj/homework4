

/** Quaternions. Basic operations. */
public class Quaternion {
    static private final double zero=0.000000001;
    private final double a,b,c,d;


   // TODO!!! Your fields here!

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
      this.a=a;
       this.b=b;
       this.c=c;
       this.d=d;
      // TODO!!! Your constructor here!
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {

       return a; // TODO!!!
   }

   /** Imaginary part i of the quaternion.
    * @return imaginary part i
    */
   public double getIpart() {

       return b; // TODO!!!
   }

   /** Imaginary part j of the quaternion.
    * @return imaginary part j
    */
   public double getJpart() {

       return c; // TODO!!!
   }

   /** Imaginary part k of the quaternion.
    * @return imaginary part k
    */
   public double getKpart() {

       return d; // TODO!!!
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion:
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {

       return a + "+" + b +"i +" + c +"j +" + d + "k"; // TODO!!!
   }

   /** Conversion from the string to the quaternion.
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) throws IllegalArgumentException {
       double a=0;
       double b=0;
       double c=0;
       double d=0;
       if(s.isEmpty()){
          // System.out.println("You entered an empty string!");
           throw new  IllegalArgumentException("You entered an empty string!" +s);
       }
       String[] tmp = s.split("\\+");
       if(tmp.length!=4){
          // System.out.println("Your String" + s +"is invalid quaternion!");
           throw  new  IllegalArgumentException("Your String" + s +"is invalid quaternion!");
       }else{ try {
           a=Double.parseDouble(tmp[0]);
           b=Double.parseDouble(tmp[1].replace("i",""));
           c=Double.parseDouble(tmp[2].replace("j",""));
           d=Double.parseDouble(tmp[3].replace("k",""));
           }
       catch (NumberFormatException e){
          // System.out.println("Invalid input caused an error:");
           //System.out.println(e);
           throw new  IllegalArgumentException("Unexpected symbol! Please check your String: " +s);
       }

       }
       return new Quaternion(a,b,c,d);// TODO!!!
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
       Quaternion copy= new Quaternion(a,b,c,d);
      return copy; // TODO!!!
   }

   /** Test whether the quaternion is zero.
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {

      return equals(new Quaternion(0.,0.,0.,0.)); // TODO!!!
   }

   /** Conjugate of the quaternion. Expressed by the formula
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {

       return new Quaternion(a, -b, -c, -d); // TODO!!!
   }

   /** Opposite of the quaternion. Expressed by the formula
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {

       return new Quaternion(-a,-b,-c,-d); // TODO!!!
   }

   /** Sum of quaternions. Expressed by the formula
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
       double a2 =q.getRpart();
       double b2 =q.getIpart();
       double c2 =q.getJpart();
       double d2 =q.getKpart();

       return new Quaternion(a+a2,b+b2,c+c2,d+d2); // TODO!!!
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
       double a2 =q.getRpart();
       double b2 =q.getIpart();
       double c2 =q.getJpart();
       double d2 =q.getKpart();

       double x0=a*a2-b*b2-c*c2-d*d2;
       double x1=a*b2+b*a2+c*d2-d*c2;
       double x2 =a*c2-b*d2+c*a2+d*b2;
       double x3=a*d2+b*c2-c*b2+d*a2;

      return new Quaternion(x0,x1,x2,x3); // TODO!!!
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {

       return new Quaternion(a*r,b*r,c*r,d*r); // TODO!!!
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
       if(this.isZero()) throw new RuntimeException("Division by zero is not allowed!");
      double k=a*a+b*b+c*c+d*d;
       return new Quaternion(a/k,-b/k,-c/k,-d/k);
        // TODO!!!
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {

       return plus(q.opposite()); // TODO!!!
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
       if (q.isZero()){
           //System.out.println("Division by zero is not allowed! "+q);
           throw new RuntimeException("Division by zero is not allowed!");
       }

       return this.times(q.inverse()); // TODO!!!
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
       if (q.isZero()){
           //System.out.println("Division by zero is not allowed! "+q);
       throw new RuntimeException("Division by zero is not allowed!");}


       return q.inverse().times(this); // TODO!!!
   }

   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
       if (qo instanceof Quaternion)
           return (Math.abs(a - ((Quaternion) qo).getRpart()) < zero
                   && Math.abs(b - ((Quaternion) qo).getIpart()) < zero
                   && Math.abs(c - ((Quaternion) qo).getJpart()) < zero
                   && Math.abs(d - ((Quaternion) qo).getKpart()) < zero)
                   ? true : false;
       else

           return false;
   }
       // TODO!!!


   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {

       return this.times(q.conjugate()).plus(q.times(this.conjugate())).times(0.5); // TODO!!!
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {

       return toString().hashCode(); // TODO!!!
   }

   /** Norm of the quaternion. Expressed by the formula
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {

       return Math.sqrt(a*a+b*b+c*c+d*d); // TODO!!!
   }

   /** Main method for testing purposes.
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: "
         + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));
   }
}
// end of file
